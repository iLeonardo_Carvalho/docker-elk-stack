![ELK Stack Logo](https://www.stratalux.com/wp-content/uploads/2016/06/Will-Migrating-to-the-Cloud-Save-Money-5.png)




Docker ELK/Source
===================

Este container contém o source do **ELK Stack** e é o executavel (binário) e são executado através do **shell script init** usando a base original do Desenvolvedor.

------



[TOC]

## Requisitos

Foi utilizado a versão **Release** acessado pelo site [Oficial do Elastic](https://www.elastic.co/) no dia **15 de julho de 2016** por alguns problemas de compatibilidade entre o **ELK** e o plugin **Collectd** e testado no dia 12 de Outubro de 2016.

> **Notas:**

> - [ELASTICSEARCH](#ELASTICSEARCH): v2.3.4
> - [LOGSTASH](#LOGSTASH): v2.3.4
> - [KIBANA](#KIBANA): v4.5.3
> - [JAVA](#JAVA): v7u80 (Repositório)
> - [COLLECTD](#COLLECTD): v5.5.2 (Repositório)

> **Obs.:**

> - Baseado no SO Debian/Ubuntu
> - LATEST (últimas atualizações de acordo com o acesso)
> - Comentado e adaptado ao LSB (Não testado o systemd)
> - Recomendado o **JAVA v7** pois o site Oficial do Elastic recomenda se for usar o JDK da Oracle
> - Container mais de um serviço é recomendado o uso do **supervisord** de acordo na documentação do [Docker](https://docs.docker.com/engine/admin/using_supervisord/)

#### Estrutura

**ELK**
```
/:.
|
\---opt
 	|
 	+---elasticsearch
 	|	|
 	|	\---config
 	|		|
 	|		|	elasticsearch.yml
 	|		+\	...
 	|
    +---logstash
    |	|
    |	+---conf.d
    |	|	|
    |	|	|	collectd.conf
    |	|	|	logstash.conf
    |	|	+\	...
    |	|
    |	\---patterns
    |		|
    |		+\	...
    |
    \---kibana
    	|
    	\---config
    		|
    		|	kibana.yml
    		+\	...
```
**LOG's**
```
/:.
|
+---opt
| 	|
| 	\---logs
| 		|
| 		+---elasticsearch
| 		|	|
| 		|	+\	...
| 		|
| 		+---logstash
| 		|	|
| 		|	+\	...
|		|
| 		\---kibana
| 			|
| 			+\	...
|
\---var
	|
	\---log
		|
		\---supervisor
			|
			+\	...
```
**DEFAULT LAYOUT**
```
Scripts 				:: 	/etc/init.d/
Supervisor Config 		:: 	/etc/supervisor/conf.d/
start/stop 				:: 	/usr/bin/supervisord -n -c /etc/supervisor/supervisord.conf

# PORTAS
9300+9200 				:: 	Elasticsearch (Consulta)
5000 					:: 	Logstash
5601 					:: 	Kibana (Web)
25826 					:: 	Collectd (Via UDP)
```
> **Obs.:** Verifique a pasta [supervisor](#supervisor), dentro dela contém os arquivos de Configuração para chamar os Scripts e às alterações dos Log's ou permissões deve-se verificar no **default.conf**.

----------


Comandos Básicos
-------------------

#### Uso

Utilize:

Para construir a partir do Código Fonte:

```
docker build -t ileonardo/elk .
```

> **Obs:** O ponto final significa que o **Dockerfile** esta na pasta atual.

Ou se preferir baixar pelo [Docker Hub](https://hub.docker.com/explore/):

```
docker pull ileonardo/elk
```

Para Executar:

```
docker run -d -p 9300:9300 -p 9200:9200 -p 5000:5000 -p 5601:5601 ileonardo/elk
```

Entrar no Container em daemon:

```
docker exec -it <ID_Container> /bin/bash
```

Sair sem encerrar o Container:

```
[CTRL] p + q
```

#### Essenciais

Ver todas as Imagens no **HOST**:

```
docker images
```

Ver todos os Containers no **HOST**:

```
docker ps -a
```

Remover uma Imagem no **HOST**:

```
docker rmi <nome_imagem>
```

Remover um Container no **HOST**:

```
docker rm <Nome_Container>
```

Remover *dangling images* (Imagens sem TAG, quer dizer quando rodou o **Dockerfile** que falhou ele cria uma imagem <none>)

```
docker rmi -f $(docker images | grep "<none>" | awk "{print \$3}")
```

Remover o histórico dos comandos do Container no **HOST**:

```
docker stop $(docker ps -a -q) && docker rm $(docker ps -a -q)
```

Remover Todas às Imagens e Containers no **HOST**:

```
docker stop $(docker ps -a -q) && \
docker rm $(docker ps -a -q) && \
docker rmi $(docker images -q)
```

#### Manipulação de Dados

Copiar um arquivo do Container para o **HOST**:

```
docker cp <ID_Container>:/caminho/no/container/arquivo /caminho/no/host
```

Copiar um arquivo do **HOST** para o Container:

```
docker cp /caminho/no/host/arquivo <ID_Container>:/caminho/no/container
```

#### Monitoramento de Containers

Para ver as estatísticas de um Container específico no **HOST**:

```
docker stats <Nome_container>
```

Para ver as estatísticas de **todos** Containers no **HOST**:

```
docker stats `docker ps | tail -n+2 | awk '{ print $NF }'`
```


Direitos autorais e Licença
-------------

Este trabalho não foi modificado de seus Criadores (Link's de consulta abaixo), foi adaptado de acordo com a documentação do mesmo e dando os créditos contida neste repositório, a busca e a organização para futuras atualizações deve ser dado ao **contributors.txt** (BY).

Este trabalho foi escrito por Leonardo Cavalcante Carvalho e está licenciado com uma [Licença **Apache-2.0**](https://www.apache.org/licenses/LICENSE-2.0).

[^stackedit]: [StackEdit](https://stackedit.io/) is a full-featured, open-source Markdown editor based on PageDown, the Markdown library used by Stack Overflow and the other Stack Exchange sites.
[^docker]: A instalação foi utilizado no domínio do [Docker](https://get.docker.com/) que está contido o script auto installer.
[^ELK]: A instalação Source das dependências foram baseadas no site [Mitesh Shah](https://miteshshah.github.io/linux/elk/).
[^Mundo Docker]: Configuração e a descrição do comando documentada baseado nos tutoriais do [Mundo Docker](www.mundodocker.com.br).